{
  description = "Extract Flash .swf files from Windows .exe files";
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    naersk = {
      url = "github:nmattia/naersk";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, naersk }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
        naersk' = pkgs.callPackage naersk { };
      in
      rec {
        packages.exe2swf = naersk'.buildPackage {
          root = ./.;
        };
        packages.default = packages.exe2swf;
        apps.exe2swf = flake-utils.lib.mkApp {
          drv = packages.exe2swf;
        };
        apps.default = apps.exe2swf;
        devShells.default = pkgs.mkShell {
          name = "exe2swf-env";
          buildInputs = [
            pkgs.cargo
            pkgs.rust-analyzer
            pkgs.rustc
          ];
        };
      }
    );
}
