//! Extract embedded SWF files from Projector EXE files

use std::fs::File;
use std::io::{Read, Seek, SeekFrom};
use std::path::PathBuf;

/// Magic bytes that must be present at the start of a Projector EXE file trailer staring at 8 bytes from EOF
static PROJECTOR_MAGIC: u32 = 0xFA123456;

/// Magic bytes that must be present at the start of an SWF file
static SWF_MAGIC: [&[u8]; 3] = [b"CWS", b"FWS", b"ZWS"];

/// Parse command line args and return the input and the output filename
fn parse_args() -> (PathBuf, PathBuf) {
    let mut args = std::env::args_os();
    if args.len() < 2 || args.len() > 3 {
        let program_name = &args.next().expect("program should have a name");
        eprintln!(
            "{app_name} {version}\nUsage: {program_name} <input.exe> [output.swf]",
            app_name = env!("CARGO_PKG_NAME"),
            version = env!("CARGO_PKG_VERSION"),
            program_name = program_name.to_string_lossy(),
        );
        std::process::exit(1)
    }
    let input_filename = args.nth(1).expect("input filename").into();
    let output_filename = args.next().map_or_else(
        || PathBuf::from(&input_filename).with_extension("swf"),
        PathBuf::from,
    );
    (input_filename, output_filename)
}

/// Parse file trailer and compute the start offset and the size of the embedded SWF data
fn get_swf_position(input: &mut File) -> Result<(u64, u64), Box<dyn std::error::Error>> {
    let mut buf = [0u8; 8];

    input.seek(SeekFrom::End(-8))?;
    let swf_end = input.stream_position()?;
    input.read_exact(&mut buf)?;
    let magic = u32::from_le_bytes(buf[..4].try_into()?);
    if magic != PROJECTOR_MAGIC {
        eprintln!("input file is not a valid projector executable",);
        std::process::exit(1)
    }
    let swf_size = u64::from(u32::from_le_bytes(buf[4..].try_into()?));
    let swf_start = swf_end
        .checked_sub(swf_size)
        .expect("SWF size cannot be larger than file size");
    Ok((swf_start, swf_size))
}

/// Check SWF magic bytes
fn check_swf_magic(input: &mut File, swf_start: u64) -> Result<(), Box<dyn std::error::Error>> {
    input.seek(SeekFrom::Start(swf_start))?;
    let mut swf_magic = [0u8; 3];
    input.read_exact(&mut swf_magic)?;
    assert!(
        SWF_MAGIC.iter().any(|&magic| { swf_magic == magic }),
        "SWF magic bytes not found"
    );
    Ok(())
}

/// Copy SWF data from the input file to the output file
fn copy_swf(
    input: &mut File,
    output: &mut File,
    swf_start: u64,
    swf_size: u64,
) -> Result<(), Box<dyn std::error::Error>> {
    input.seek(SeekFrom::Start(swf_start))?;
    let mut swf_input = input.take(swf_size);
    std::io::copy(&mut swf_input, output)?;
    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let (input_filename, output_filename) = parse_args();

    let mut input = File::open(input_filename)?;
    let (swf_start, swf_size) = get_swf_position(&mut input)?;

    check_swf_magic(&mut input, swf_start)?;

    let mut output = File::create(output_filename)?;
    copy_swf(&mut input, &mut output, swf_start, swf_size)?;
    Ok(())
}
