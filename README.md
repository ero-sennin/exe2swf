# exe2swf

[![Crates.io](https://img.shields.io/crates/v/exe2swf.svg)](https://crates.io/crates/exe2swf)

Extract Flash .swf files from Windows .exe files.

## Links:
- [http://www.nullsecurity.org/article/extracting_swf_from_flash_projector](http://www.nullsecurity.org/article/extracting_swf_from_flash_projector)
- [https://github.com/laenion/dump_projector](https://github.com/laenion/dump_projector)
